from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views



# Create your tests here.

class Story7UnitTest(TestCase):

    def test_does_landing_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story7app/index.html')

    def test_func_page(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

