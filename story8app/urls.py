from django.urls import path
from . import views 
# app_name = 'story8app'

urlpatterns = [
    path('', views.index, name='searchBook'),
    path('ajax/<int:start_index>/<str:book_name>/', views.getApi, name='ajaxSearch')
]
