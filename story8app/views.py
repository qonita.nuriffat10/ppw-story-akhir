from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
# import pip._vendor.requests as requests
import requests as r


# Create your views here.


def index(request):
    return render(request, 'story8app/index.html')


def getApi(request, book_name, start_index):

    a = r.get(
        "https://www.googleapis.com/books/v1/volumes?q="+book_name+"&startIndex="+str(10*(start_index - 1)))
    return JsonResponse(a.json())
