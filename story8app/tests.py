from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps

from . import views
from .apps import Story8AppConfig


# class TestApp(TestCase):
#     def test_apps(self):
#         self.assertEqual(Story8AppConfig.name, 'story8app')

class UnitTest(TestCase):
    def test_views_index_exists(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_index_url(self):
        found = resolve(reverse('searchBook'))
        self.assertTrue(found.func, views.index)

    def test_ajax_url(self):
        found = resolve(reverse('ajaxSearch', args=[1, 'lol']))
        self.assertTrue(found.func, views.getApi)

    def test_views_ajax(self):
        response = Client().get('/story8/ajax/1/lol')
        self.assertEqual(response.status_code, 301)
        #
