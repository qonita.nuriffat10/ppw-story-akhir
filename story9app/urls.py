from django.urls import path, include
from .views import story9_index, logout_story9, register

#app_name = "story9app"

urlpatterns = [
    path('', story9_index, name='story9_index'),
    path('logout/', logout_story9, name='logout'),
    path('register/', register, name='register'),
]
